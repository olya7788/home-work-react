import " ./List.scss";
import " ../listitem/ListItem.js";
import ListItem from " ../listitem/ListItem.js";

function List({ List = []}) {
    return (
        <div className={'List'}>
           {
            List.map((item) => {
                return <ListItem key={item.id} img={item.image} title={item.title} price={item.price}></ListItem> 
            })
           }
        </div>
    )
}

export default List;
